## Virtual Fridge

This Virtual Fridge project is a Vue.js application designed to manage the contents of a virtual refrigerator. It allows users to add, delete, update, and search for products stored in the fridge.

### Components

Vue template files in the `src/components` folder are automatically imported and registered as global components. This means that you can use any component in your application without having to manually import it.

#### 🚀 Usage

The `unplugin-vue-components` plugin handles the automatic importation of `.vue` files created in the `src/components` directory.

For example, assuming a component located at `src/components/MyComponent.vue`:

```vue
<template>
  <div>
    <MyComponent />
  </div>
</template>

<script setup>
  //
</script>
```

When your template is rendered, the component's import will automatically be inlined, rendering to this:

```vue
<template>
  <div>
    <MyComponent />
  </div>
</template>

<script setup>
  import MyComponent from '@/components/MyComponent.vue'
</script>
```

This setup reduces code duplication and simplifies component usage in your Vue files.

### Installation

To set up the Virtual Fridge project locally, follow these steps:

1. Clone the repository:

```bash
git clone https://github.com/your-username/virtual-fridge.git
```

2. Navigate to the project directory:

```bash
cd virtual-fridge
```

3. Install dependencies:

```bash
npm install
```

4. Run the development server:

```bash
npm run serve
```

### Features

- **Add Products:** Users can add new products to the virtual fridge.
- **Delete Products:** Products can be removed from the fridge.
- **Update Products:** Users can update the quantity of products in the fridge.
- **Search:** The application provides a search functionality to find specific products.
- **Responsive Design:** The application is designed to work seamlessly across different screen sizes.

### Technologies Used

- **Vue.js:** The project is built using the Vue.js framework for building user interfaces.
- **Axios:** Axios is used for making HTTP requests to fetch and update product data.
- **Vue Router:** Vue Router is used for handling client-side routing in the application.
- **Vuetify:** Vuetify is used for styling and UI components, providing a clean and responsive design.
- **Vuex:** Vuex is used for state management, allowing for centralized data management in the application.

### Contribution

Contributions are welcome! If you have any suggestions, bug reports, or feature requests, please open an issue or submit a pull request.

### License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.