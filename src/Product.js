export default class Produit {
  constructor(produitJSON) {
    this._id = produitJSON.id;
    this._nom = produitJSON.nom;
    this._qte = produitJSON.qte;
    this._photo = produitJSON.photo ?? "";
  }

  // Getter methods for accessing private properties
  get id() {
    return this._id;
  }

  get nom() {
    return this._nom;
  }

  get qte() {
    return this._qte;
  }

  get photo() {
    return this._photo;
  }

  // Method to return a string representation of the product
  toString() {
    return `--> ${this._nom} (Quantité: ${this._qte})`;
  }

  // Method to increase the quantity of the product
  increaseQty() {
    this._qte += 1;
  }

// Method to decrease the quantity of the product (if greater than 0)
  decreaseQty() {
    if (this._qte > 0) this._qte -= 1;
  }

  // Method to convert the object to JSON format for API communication
  toJSON() {
    return {
      id: this._id,
      nom: this._nom,
      qte: this._qte,
      photo: this._photo
    };
  }
}
